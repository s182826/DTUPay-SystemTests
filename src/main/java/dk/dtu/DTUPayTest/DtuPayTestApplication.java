package dk.dtu.DTUPayTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DtuPayTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtuPayTestApplication.class, args);
	}

}
