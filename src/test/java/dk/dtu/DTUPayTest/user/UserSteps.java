package dk.dtu.DTUPayTest.user;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.dtu.DTUPayTest.user.dto.CprNumber;
import dk.dtu.DTUPayTest.user.dto.Customer;
import dk.dtu.DTUPayTest.user.dto.DTUPayUser;
import dk.dtu.DTUPayTest.user.dto.Merchant;
import org.assertj.core.api.Assertions;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 *  @author Daniel Szajcz (s192623)
 */
public class UserSteps extends UserControllerTest {

    private Customer customer;
    private Merchant merchant;

    private String customerCpr;
    private String merchantCpr;

    private String modification = "modification";

    @Given("^a customer$")
    public void aCustomer() {
        customerCpr = UUID.randomUUID().toString();
        customer = new Customer(
                new CprNumber(customerCpr),
                "test",
                "test",
                "test"
        );
    }

    @When("^adding the customer to the system$")
    public void addingTheCustomerToTheSystem() {
        saveCustomer(customer);
    }

    @Then("the customer gets saved")
    public void theCustomerGetsSaved() {
        DTUPayUser requestedCustomer = getUserById(customer.getCprNumber().getNumber());
        assertEquals(customer, requestedCustomer);
    }

    @And("modifying that customer")
    public void modifyingThatCustomer() {
        DTUPayUser customerToUpdate = getUserById(customer.getCprNumber().getNumber());
        customerToUpdate.setLastName(modification);
        updateUser(customerToUpdate);
    }

    @Then("the customer gets modified")
    public void theCustomerGetsModified() {
        DTUPayUser updatedCustomer = getUserById(customer.getCprNumber().getNumber());
        assertEquals(modification, updatedCustomer.getLastName());
    }

    @Given("^a merchant$")
    public void aMerchantWith() {
        merchantCpr = UUID.randomUUID().toString();
        merchant = new Merchant(
                new CprNumber(merchantCpr),
                "test",
                "test",
                "test"
        );
    }

    @When("adding the merchant to the system")
    public void addingTheMerchantToTheSystem() {
        saveMerchant(merchant);
    }

    @Then("the merchant gets saved")
    public void theMerchantGetsSaved() {
        DTUPayUser requestedMerchant = getUserById(merchant.getCprNumber().getNumber());
        assertEquals(merchant, requestedMerchant);
    }

    @And("modifying that merchant")
    public void modifyingThatMerchant() {
        DTUPayUser merchantToUpdate = getUserById(merchant.getCprNumber().getNumber());
        merchantToUpdate.setLastName(modification);
        updateUser(merchantToUpdate);
    }

    @Then("the merchant gets modified")
    public void theMerchantGetsModified() {
        DTUPayUser updatedCustomer = getUserById(merchant.getCprNumber().getNumber());
        assertEquals(modification, updatedCustomer.getLastName());
    }

    @And("delete the customer from the system")
    public void deletingTheCustomerFromTheSystem() {
        DTUPayUser customerToDelete = getUserById(customer.getCprNumber().getNumber());
        deleteUser(customerToDelete.getCprNumber().getNumber());
    }


    @Then("the customer gets deleted")
    public void theCustomerGetsDeleted() {
        Assertions.assertThatThrownBy(() -> getUserById(customer.getCprNumber().getNumber())).
                isInstanceOf(Throwable.class);
    }

    @And("delete the merchant from the system")
    public void deletingTheMerchantFromTheSystem() {
        DTUPayUser merchantToDelete = getUserById(merchant.getCprNumber().getNumber());
        deleteUser(merchantToDelete.getCprNumber().getNumber());
    }


    @Then("the merchant gets deleted")
    public void theMerchantGetsDeleted() {
        Assertions.assertThatThrownBy(() -> getUserById(merchant.getCprNumber().getNumber())).
                isInstanceOf(Throwable.class);
    }
}
