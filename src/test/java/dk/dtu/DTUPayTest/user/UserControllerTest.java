package dk.dtu.DTUPayTest.user;
import dk.dtu.DTUPayTest.common.BaseControllerTest;
import dk.dtu.DTUPayTest.user.dto.Customer;
import dk.dtu.DTUPayTest.user.dto.DTUPayUser;
import dk.dtu.DTUPayTest.user.dto.Merchant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;

/**
 *  @author Daniel Szajcz (s192623)
 */
public class UserControllerTest extends BaseControllerTest {

    @Value("${service.user.url}")
    private String userUrl;

    protected void saveCustomer(Customer customer) {
        client.post()
                .uri(userUrl + "customer")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(customer)
                .exchange()
                .expectStatus().isOk();
    }

    protected void saveMerchant(Merchant merchant) {
        client.post()
                .uri(userUrl + "merchant")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(merchant)
                .exchange()
                .expectStatus().isOk();
    }

    protected DTUPayUser getUserById(String cprNumber) {
        return client.get()
                .uri(userUrl + cprNumber)
                .exchange()
                .expectBody(DTUPayUser.class).returnResult().getResponseBody();
    }

    protected void updateUser(DTUPayUser updatedCustomer) {
        client.put()
                .uri(userUrl )
                .syncBody(updatedCustomer)
                .exchange()
                .expectStatus().isOk();
    }

    protected void deleteUser(String cprNumber) {
        client.delete()
                .uri(userUrl + cprNumber)
                .exchange()
                .expectStatus().isOk();
    }

}
