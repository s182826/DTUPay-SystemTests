package dk.dtu.DTUPayTest.user.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Customer extends DTUPayUser{
    public Customer(CprNumber cprNumber, String firstName, String lastName, String accountNo) {
        super(cprNumber, firstName, lastName, accountNo);
        type = "Customer";
    }
}