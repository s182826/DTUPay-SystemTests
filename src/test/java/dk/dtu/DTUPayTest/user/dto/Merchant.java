package dk.dtu.DTUPayTest.user.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Merchant extends DTUPayUser{
    public Merchant(CprNumber cprNumber, String firstName, String lastName, String accountNo){
        super(cprNumber, firstName, lastName, accountNo);
        type = "Merchant";
    }
}