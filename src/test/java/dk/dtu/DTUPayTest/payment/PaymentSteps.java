package dk.dtu.DTUPayTest.payment;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.dtu.DTUPayTest.payment.dto.PaymentDTO;
import dk.dtu.DTUPayTest.payment.dto.Transaction;
import dk.dtu.DTUPayTest.token.TokenControllerTest;
import dk.dtu.DTUPayTest.token.dto.Token;
import dk.dtu.DTUPayTest.user.dto.CprNumber;
import dk.dtu.DTUPayTest.user.dto.Customer;
import dk.dtu.DTUPayTest.user.dto.Merchant;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 *  @author Bence Burom (s192620)
 */
public class PaymentSteps extends PaymentControllerTest {

    private List<Token> newTokens;
    private PaymentDTO paymentDTO;

    private Customer customer;
    private Merchant merchant;

    private String customerCpr;
    private String merchantCpr;

    @And("^generate (\\d+) tokens for the customer")
    public void generateTokensForTheCustomerWith(int amount) {
        newTokens = generateTokens(amount, customerCpr);
    }

    @And("a payment")
    public void aPaymentWithAnd() {
        paymentDTO = new PaymentDTO(customerCpr, merchantCpr, newTokens.get(1).getUuid().toString(), 5, "");
    }


    @And("^wait (\\d+) sec$")
    public void waitSec(int seconds) throws InterruptedException {
        TimeUnit.SECONDS.sleep(seconds);
    }

    @When("^making the payment$")
    public void makingThePayment() {
        makePayment(paymentDTO);
    }

    @Then("^transaction happens")
    public void transactionHappens() {
        Set<Transaction> transactions = getTransactions(customerCpr);
        assertEquals(1, transactions.size());
    }

    @Given("a merchant for payment")
    public void aMerchantForPayment() {
        merchantCpr = UUID.randomUUID().toString();
        merchant = new Merchant(
                new CprNumber(merchantCpr),
                "test",
                "test",
                "test"
        );
    }

    @Given("a customer for payment")
    public void aCustomerForPayment() {
        customerCpr = UUID.randomUUID().toString();
        customer = new Customer(
                new CprNumber(customerCpr),
                "test",
                "test",
                "test"
        );
    }
}
