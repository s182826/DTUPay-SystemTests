package dk.dtu.DTUPayTest.payment.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

@Data
@NoArgsConstructor
public class Transaction {
    private UUID id;
    private double amount;
    private String debtorCprNumber;
    private String creditorCprNumber;
    private LocalDate date;

    public Transaction(double amount, String debtorCprNumber, String creditorCprNumber, LocalDate date) {
        this.id = UUID.randomUUID();
        this.amount = amount;
        this.debtorCprNumber = debtorCprNumber;
        this.creditorCprNumber = creditorCprNumber;
        this.date = date;
    }

    public boolean dateInRange(LocalDate startDate, LocalDate endDate){
        return startDate.isBefore(this.date) && endDate.isAfter(this.date);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", customerCprNumber='" + debtorCprNumber + '\'' +
                ", merchantCprNumber='" + creditorCprNumber + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
