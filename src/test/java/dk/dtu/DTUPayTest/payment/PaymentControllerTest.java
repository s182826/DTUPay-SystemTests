package dk.dtu.DTUPayTest.payment;

import dk.dtu.DTUPayTest.common.BaseControllerTest;
import dk.dtu.DTUPayTest.payment.dto.PaymentDTO;
import dk.dtu.DTUPayTest.payment.dto.RefundDTO;
import dk.dtu.DTUPayTest.payment.dto.Transaction;
import dk.dtu.DTUPayTest.token.dto.Token;
import dk.dtu.DTUPayTest.user.dto.Customer;
import dk.dtu.DTUPayTest.user.dto.Merchant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *  @author Bence Burom (s192620)
 */
public class PaymentControllerTest extends BaseControllerTest {

    @Value("${service.payment.url}")
    private String paymentUrl;

    @Value("${service.report.url}")
    private String reportUrl;

    @Value("${service.user.url}")
    private String userUrl;

    protected void makePayment(PaymentDTO paymentDTO) {
        client.post()
                .uri( paymentUrl + "/pay")
                .syncBody(paymentDTO)
                .exchange()
                .expectStatus().isOk();
    }

    protected void makeRefund(RefundDTO refundDTO) {
        client.post()
                .uri(paymentUrl + "/refund")
                .syncBody(refundDTO)
                .exchange()
                .expectStatus().isOk();
    }


    public Set<Transaction> getTransactions(String userId) {
        List<Transaction> result = client.get()
                .uri(reportUrl + "/reports/customer/" + userId + "?startDate=" + LocalDateTime.now().minusDays(1) + "&endDate=" + LocalDateTime.now().plusDays(1))
                .exchange()
                .expectBodyList(Transaction.class).returnResult().getResponseBody();

        return new HashSet<>(result);
    }

    public List<Token> generateTokens(int amount, String userId) {
        return client.put()
                .uri("http://localhost:8989/api/tokens/" + amount + "/users/" + userId)
                .exchange()
                .expectStatus().isCreated()
                .expectBodyList(Token.class).returnResult().getResponseBody();
    }

    protected void saveCustomer(Customer customer) {
        client.post()
                .uri(userUrl + "customer")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(customer)
                .exchange()
                .expectStatus().isOk();
    }

    protected void saveMerchant(Merchant merchant) {
        client.post()
                .uri(userUrl + "merchant")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(merchant)
                .exchange()
                .expectStatus().isOk();
    }

}
