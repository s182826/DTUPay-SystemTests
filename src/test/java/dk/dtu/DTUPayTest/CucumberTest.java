package dk.dtu.DTUPayTest;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/dk/dtu/DTUPayTest/features",
        monochrome = true,
        snippets = SnippetType.CAMELCASE
)
public class CucumberTest {

}
