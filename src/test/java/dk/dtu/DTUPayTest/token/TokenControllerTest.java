package dk.dtu.DTUPayTest.token;

import dk.dtu.DTUPayTest.common.BaseControllerTest;
import dk.dtu.DTUPayTest.token.dto.Token;
import org.springframework.beans.factory.annotation.Value;
import java.util.List;

/**
 *  @author Janos Richard Pekk (s192617)
 */
public class TokenControllerTest extends BaseControllerTest {

    @Value("${service.token.url}")
    private String tokenUrl;

    public List<Token> generateTokens(int amount, String userId) {
        return client.put()
                .uri(tokenUrl + amount + "/users/" + userId)
                .exchange()
                .expectStatus().isCreated()
                .expectBodyList(Token.class).returnResult().getResponseBody();
    }

    public void useToken(String tokenId, String userId) {
        client.put()
                .uri(tokenUrl + tokenId + "/users/" + userId + "/use")
                .exchange()
                .expectStatus().isOk();
    }
}
