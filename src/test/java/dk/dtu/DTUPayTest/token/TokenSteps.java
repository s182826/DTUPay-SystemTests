package dk.dtu.DTUPayTest.token;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import dk.dtu.DTUPayTest.token.dto.Token;
import org.assertj.core.api.Assertions;


import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 *  @author Janos Richard Pekk (s192617)
 */
public class TokenSteps extends TokenControllerTest {

    private String userId;
    private int amount;
    private List<Token> newTokens;

    @Given("^an amount$")
    public void an_amount() {
        amount = 5;
    }

    @Then("^the given amount of tokens appear$")
    public void the_given_amount_of_tokens_appear() {
        assertEquals(amount, newTokens.size());
    }

    @And("^generating tokens for the customer$")
    public void generatingTokensForTheCustomer() {
        userId = UUID.randomUUID().toString();
        newTokens = generateTokens(amount, userId);
    }

    @Then("^using a token is successful$")
    public void usingATokenIsSuccessful() {
        useToken(newTokens.get(1).getUuid().toString(), userId);
    }

}
