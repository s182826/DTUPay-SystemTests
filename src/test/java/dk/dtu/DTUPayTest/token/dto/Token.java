package dk.dtu.DTUPayTest.token.dto;

import java.util.Objects;
import java.util.UUID;

public class Token {
    private UUID uuid;

    public Token() {
        this.uuid = UUID.randomUUID();
    }

    public Token(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return uuid.equals(token.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}