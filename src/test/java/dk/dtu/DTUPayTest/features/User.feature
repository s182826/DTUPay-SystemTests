Feature: User
  Description: Managing users

  Scenario: Creating a new customer
    Given a customer
    When adding the customer to the system
    Then the customer gets saved
    And delete the customer from the system

  Scenario: Modifying a customer
    Given a customer
    When adding the customer to the system
    And modifying that customer
    Then the customer gets modified
    And delete the customer from the system

  Scenario: Creating a new merchant
    Given a merchant
    When adding the merchant to the system
    Then the merchant gets saved
    And delete the merchant from the system

  Scenario: Modifying a merchant
    Given a merchant
    When adding the merchant to the system
    And modifying that merchant
    Then the merchant gets modified
    And delete the merchant from the system

  Scenario: Delete customer
     Given a customer
     When adding the customer to the system
     And delete the customer from the system
     Then the customer gets deleted

  Scenario: Delete merchant
    Given a merchant
    When adding the merchant to the system
    And delete the merchant from the system
    Then the merchant gets deleted

