Feature: Token
  Description: Managing tokens

  Scenario: Generating tokens
    Given an amount
    And generating tokens for the customer
    Then the given amount of tokens appear

  Scenario: Using a token
    Given an amount
    And generating tokens for the customer
    Then using a token is successful
