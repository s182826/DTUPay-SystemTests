Feature: Payment
  Description: Managing payments

  Scenario: Making a payment
    Given a customer for payment
    And a merchant for payment
    And generate 5 tokens for the customer
    And a payment
    When making the payment
    And wait 5 sec
    Then transaction happens
